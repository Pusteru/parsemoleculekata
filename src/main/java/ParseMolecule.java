import javafx.util.Pair;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParseMolecule {

    private static final Set<String> atomNames = new HashSet<>(Arrays.asList("H", "He", "Li", "Be", "B", "C", "N", "O"
            ,"F","Ne","Na","Mg","Al","Si","P","S","Cl","Ar","K","Ca","Sc","Ti","V","Cr","Mn","Fe","Co","Ni","Cu","Zn"
            ,"Ga","Ge","As","Se","Br","Kr","Rb","Sr","Y","Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag","Cd","In","Sn","Sb"
            ,"Te","I","Xe","Cs","Ba","La","Ce","Pr","Nd","Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf"
            ,"Ta","W","Re","Os","Ir","Pt","Au","Hg","Tl","Pb","Bi","Po","At","Rn","Fr","Ra","Ac","Th","Pa","U","Np"
            ,"Pu","Am","Cm","Bk","Cf","Es","Fm","Md","No","Lr","Rf","Db","Sg","Bh","Hs","Mt","Ds","Rg","Cn","Nh","Fl"
            , "Mc", "Lv", "Ts", "Og", ""));

    private static final Set<String> OPENING_BRACKETS = new HashSet<>(Arrays.asList("(", "[", "{"));
    private static final Set<String> CLOSING_BRACKETS = new HashSet<>(Arrays.asList(")", "]", "}"));
    private static final String PATTERN_ATOMS_NUMBERS_BRACKETS = "(([A-Z][a-z]\\d+)|([A-Z][a-z])|[A-Z]\\d+|[A-Z]|\\d+|[\\[,\\],\\(,\\),\\{,\\}|\\D])";
    private static final String PATTERN_ALL_NUMERIC = "\\d+";
    private static final String PATTERN_ALL_NO_NUMERIC = "\\D+";


    static Map<String, Integer> getAtoms(String formula) {
        if (!checkIfCorrectBraces(formula)) throw new IllegalArgumentException();
        String[] elements = getElements(formula);
        return getAtomsRecursive(elements).getKey();
    }

    private static Pair<Map<String, Integer>, String[]> getAtomsRecursive(String[] formula) {
        Map<String, Integer> map = new HashMap<>();
        for (int i = 0; i < formula.length; ++i) {
            String atom = formula[i];
            if (OPENING_BRACKETS.contains(atom)) {
                Pair<Map<String, Integer>, String[]> res = getAtomsRecursive(Arrays.copyOfRange(formula, i + 1, formula.length));
                mergeMaps(map, res);
                i = getNewPositionAfterRecursion(formula, res);
            } else if (CLOSING_BRACKETS.contains(atom)) {
                if (nextIsMultiplier(formula, i)) {
                    int multiply = Integer.valueOf(formula[++i]);
                    map = getMultipliedMap(map, multiply);
                }
                return new Pair<>(map, Arrays.copyOfRange(formula, i, formula.length));
            } else {
                addNewElement(map, atom);
            }
        }
        return new Pair<>(map, null);

    }

    private static Map<String, Integer> getMultipliedMap(Map<String, Integer> map, int multiply) {
        Map<String, Integer> multipliedMap = new HashMap<>();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            multipliedMap.put(entry.getKey(), entry.getValue() * multiply);
        }
        return multipliedMap;
    }

    private static int getNewPositionAfterRecursion(String[] formula, Pair<Map<String, Integer>, String[]> res) {
        return formula.length - res.getValue().length;
    }

    private static void mergeMaps(Map<String, Integer> map, Pair<Map<String, Integer>, String[]> res) {
        for (Map.Entry<String, Integer> entry : res.getKey().entrySet()) {
            map.merge(entry.getKey(), entry.getValue(), Integer::sum);
        }
    }

    private static void addNewElement(Map<String, Integer> map, final String atom) {
        String value = atom.replaceAll(PATTERN_ALL_NO_NUMERIC, "");
        int number = value.isEmpty() ? 1 : Integer.valueOf(value);
        String name = atom.replaceAll(PATTERN_ALL_NUMERIC, "");
        map.merge(name, number, Integer::sum);
    }

    private static boolean nextIsMultiplier(String[] formula, int i) {
        return formula.length > i + 1 && (Character.isDigit(formula[i + 1].charAt(0)));
    }

    private static String[] getElements(String formula) {
        List<String> formulaElements = new ArrayList<>();
        Matcher matcher = Pattern.compile(PATTERN_ATOMS_NUMBERS_BRACKETS).matcher(formula);
        while (matcher.find()) {
            String atom = matcher.group();
            formulaElements.add(atom);
            if (!atomNames.contains(atom.replaceAll("\\d+|\\[|\\]|\\(|\\)|\\{|\\}", "")))
                throw new IllegalArgumentException();
        }
        return formulaElements.toArray(new String[formulaElements.size()]);
    }

    private static boolean checkIfCorrectBraces(String braces) {
        String b = braces;
        System.out.println(braces);
        for(int i=0;i<braces.length()/2;i++)
        {
            b = b.replaceAll("[^\\(,\\),\\[,\\],\\{,\\}]","");
            b = b.replaceAll("\\(\\)", "");
            b = b.replaceAll("\\[\\]", "");
            b = b.replaceAll("\\{\\}", "");
            if(b.length() == 0)
                return true;
        }
        return false;
    }

}
